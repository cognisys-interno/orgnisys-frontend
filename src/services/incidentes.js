const ENDPOINT = 'http://localhost:8080/api/incidentes'

export default function getIncidentes (fechaIni, fechaFin ) {
    const params = {
        fechaInicial: fechaIni,
        fechaFin: fechaFin
    }
    return fetch(`${ENDPOINT}/getIncidentes?` + new URLSearchParams(params), {
        method: 'GET',
        headers: {
            "Content-Type": "application/json"
        }
    }).then( res => {
        console.log("RESPUESTA: ", res.json());
        console.log("RESPUESTA2: ", res.ok);

        if (!res.ok) throw new Error('Error recuperando incidentes')
        return res.json()
    }).then( res => {
        console.log("RESPUESTA3: ", res.body);
        return res.data
    })
}