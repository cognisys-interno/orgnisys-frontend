const ENDPOINT = 'http://localhost:8080/api/desarrollos'

export default function getDesarrollos ({ fechaIni, fechaFin }) {
    const params = {
        fechaInicial: fechaIni,
        fechaFin: fechaFin
    }
    return fetch(`${ENDPOINT}/getDesarrollos?` + new URLSearchParams({params}), {
        method: 'GET',
        headers: {
            "Content-Type": "application/json"
        }
    }).then( res => {
        if (!res.ok) throw new Error('Error recuperando incidentes')
        return res.json()
    }).then( res => {
        console.log("RESPUESTA: ", res);
        return res.data
    })
}