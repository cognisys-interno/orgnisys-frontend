import React, { useEffect } from "react";
import "./pages.css";
import TabMenu from "../components/tabs/Tabs";
import { getTabsIncidencia } from "../components/tabs/TabNames";
import StaticCard from "../components/staticCard/StaticCard";
import Header from "./components/header/Header";
import Grid from "@mui/material/Grid";
import { DateContext } from "../context/dateContext/DateContext";
import { useContext } from "react";
import useTarjeta from "../hooks/UseTarjeta";

const tabs = getTabsIncidencia();

const Incidencias = ({ title, icon }) => {

  const datos = [
    { titulo: "Masivo", cantidad: 200 },
    { titulo: "Empresarial", cantidad: 200 },
    { titulo: "Script", cantidad: 200 },
    { titulo: "Correctivo", cantidad: 100 },
    { titulo: "Sin Modificacion", cantidad: 50 },
    { titulo: "Relacionado", cantidad: 50 },
  ];

  const {obtenerIncidentes} = useTarjeta();
  const dateContext = useContext(DateContext);

  useEffect(() => {
    console.log(dateContext.finalDate);
    obtenerIncidentes(dateContext.initialDate, dateContext.finalDate);
  }, [dateContext.changeFinalDate]);

  return (
    <div className="cont">
      <div className="main-container">
        <Header title={title} icon={icon} />
        <div className="content-container">
          <Grid container spacing={3} className="cont-card">
            <StaticCard data={datos} />
          </Grid>
          <div className="contenedor-tabla">
            <TabMenu tabs={tabs} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Incidencias;
