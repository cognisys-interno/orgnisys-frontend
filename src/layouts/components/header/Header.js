import React from 'react'
import SelectorFecha from '../../../components/date-selector/DateSelector'

export default function Header({title,icon}) {
  return (
    <header>
        <div className="header-container">
            <div className="title">
                <ol className="ol-title">
                <li>{icon}</li>
                <li> &nbsp; / &nbsp; </li>
                <li>{title} </li>
                </ol>
            </div>
            <SelectorFecha />
           
        </div>
  </header>

  )
}
