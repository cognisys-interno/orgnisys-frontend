import React from "react";
import "./pages.css";
import TabMenu from "../components/tabs/Tabs";
import Header from "./components/header/Header";
import StaticCard from "../components/staticCard/StaticCard";
import Grid from "@mui/material/Grid";
import Chart from "../components/column-chart/Chart";
import { DateContext } from "../context/dateContext/DateContext";
import useTarjeta from "../hooks/UseTarjeta";
import { useContext } from "react";
import { useEffect } from "react";

const tabs = ["Analisis y diseño", "Desarrollo", "Testing", "Finalizado"];
const datos = [
  { titulo: "Total desarrollos", cantidad: 200 },
  { titulo: "Pruebas reportadas", cantidad: 100 },
  { titulo: "Pruebas generadas", cantidad: 200 },
  { titulo: "Pruebas internas", cantidad: 100 },
];



const Desarrollos = ({ title, icon }) => {

  const {obtenerdesarrollos} = useTarjeta();
  const dateContext = useContext(DateContext);

  useEffect(() => {
    obtenerdesarrollos(dateContext.initialDate, dateContext.finalDate);
  }, [dateContext.changeFinalDate]);

  return (
    <div className="cont">
      <div className="main-container">
        <Header title={title} icon={icon} />
        <div className="content-container">
          <Grid container spacing={3} className="cont-card">
            <StaticCard data={datos}></StaticCard>
            <Chart></Chart>
          </Grid>

          <div className="contenedor-tabla">
            <TabMenu tabs={tabs} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Desarrollos;
