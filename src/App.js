import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Incidencias from "./layouts/Incidencias";
import Desarrollos from "./layouts/Desarrollos";
import { CustomDateContext } from "./context/dateContext/CustomDateContext";
import * as IoIcons from "react-icons/io";
import * as MdIcons from "react-icons/md";
import AsideMenu from "./components/side-menu/Sidebar";
import { TarjetaContextProvider } from "./context/TarjetasContext";

function App() {
  return (
    <BrowserRouter>
      <TarjetaContextProvider>
        <CustomDateContext>
          <AsideMenu />
          <Routes>
            <Route
              path="incidencias"
              element={
                <Incidencias
                  title="Incidentes"
                  icon={<IoIcons.IoIosPaper className="title-icon" />}
                />
              }
            />
            <Route
              path="desarrollos"
              element={
                <Desarrollos
                  title="Desarrollos"
                  icon={<MdIcons.MdOutlineComputer className="title-icon" />}
                />
              }
            />
            <Route
              path="*"
              element={
                <Incidencias
                  title="Incidentes"
                  icon={<IoIcons.IoIosPaper className="title-icon" />}
                />
              }
            />
          </Routes>
        </CustomDateContext>
      </TarjetaContextProvider>
    </BrowserRouter>
  );
}

export default App;
