import React, { useEffect, useState } from "react";
import { InputAdornment, TextField } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import * as GoIcons from 'react-icons/go'
import "../table/table.css";
import { Link } from "react-router-dom";


export default function TablaDesarrollo({ columnas, desarrollos, tab }) {
  const [filtro, setFiltro] = useState("");
  const [tablaDesarrollos, setTablaDesarrollos] = useState(desarrollos);
  const [desarrollosFiltrados, setDesarrollosFiltrados] = useState(desarrollos);

  useEffect(() => {
    setTablaDesarrollos(desarrollos);
  }, []);

  const handleFiltroChange = (e) => {
    setFiltro(e.target.value);
    filtrar(e.target.value);
    console.log("Busqueda: ", e.target.value);
  };

  const filtrar = (terminoBusqueda) => {
    var resultadoBusqueda = tablaDesarrollos.filter((elemento) => {
      if (
        elemento.nombre
          .toString()
          .toLowerCase()
          .includes(terminoBusqueda.toLowerCase())
      ) {
        return elemento;
      }
    });
    setDesarrollosFiltrados(resultadoBusqueda);
  };

  return (
    <>
      <div className="filters">
        <TextField
          id="outlined-basic"
          onChange={handleFiltroChange}
          value={filtro}
          label="Filtrar"
          variant="standard"
          autoComplete="off"
          style={{ width: "20%", marginBottom: "30px" }}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <SearchIcon />
              </InputAdornment>
            ),
          }}
        />
      </div>
      <div className="cont-tabla">
        <table>
          <thead>
            <tr>
              {columnas.map((columna, index) => (
                <th className={columna} key={index}>
                  {columna}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {desarrollosFiltrados.map((fila, index) =>
              fila.estado.toUpperCase() === tab.toUpperCase() ? (
                <tr
                  key={index}
                  //TODO, AGREGAR ONCLICK
                >
                  <td >{fila.nombre}</td>
                  <td>{fila.encargado}</td>
                  <td primary>{fila.tareas}</td>
                  <td>{fila.fecha}</td>
                  <td primary> <a href={fila.bitacora}><GoIcons.GoLinkExternal></GoIcons.GoLinkExternal></a></td>
                </tr>
              ) : (
                <></>
              )
            )}
          </tbody>
        </table>
      </div>
    </>
  );
}
