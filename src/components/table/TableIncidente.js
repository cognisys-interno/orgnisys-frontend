import React, { useEffect, useState } from "react";
import { InputAdornment, TextField } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import "./table.css";

export default function TablaIncidentes({ columnas, incidentes, tab }) {
  const [filtro, setFiltro] = useState("");
  const [tablaIncidencias, setTablaIncidencias] = useState(incidentes);
  const [incidencias, setIncidencias] = useState(incidentes);

  useEffect(() => {
    setTablaIncidencias(incidentes);
  }, []);

  const handleFiltroChange = (e) => {
    setFiltro(e.target.value);
    filtrar(e.target.value);
    console.log("Busqueda: ", e.target.value);
  };

  const filtrar = (terminoBusqueda) => {
    var resultadoBusqueda = tablaIncidencias.filter((elemento) => {
      if (
        elemento.incidencia
          .toString()
          .toLowerCase()
          .includes(terminoBusqueda.toLowerCase())
      ) {
        return elemento;
      }
    });
    setIncidencias(resultadoBusqueda);
  };

  return (
    <>
      <div className="filters">
        <TextField
          id="outlined-basic"
          onChange={handleFiltroChange}
          value={filtro}
          label="Filtrar"
          variant="standard"
          autoComplete="off"
          style={{ width: "20%", marginBottom: "30px" }}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <SearchIcon />
              </InputAdornment>
            ),
          }}
        />

        <div className="checkbox">
          <div className="masivo">
            <label htmlFor="masivos">Masivos: </label>
            <input id="masivos" type="checkbox" />
          </div>

          <div className="corpo">
            <label htmlFor="corporativos">Corporativos: </label>
            <input className="corporativos" type="checkbox" />
          </div>
        </div>
      </div>
      <div className="cont-tabla">
        <table>
          <thead>
            <tr>
              {columnas.map((columna, index) => (
                <th className={columna} key={index}>
                  {columna}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {incidencias.map((fila, index) =>
              fila.metodo === tab ? (
                <tr
                  key={index}
                  //TODO, AGREGAR ONCLICK
                >
                  <td>{fila.incidencia}</td>
                  <td className={fila.tipo}>
                    <div>{fila.tipo}</div>
                  </td>
                  <td primary>{fila.solucion}</td>
                </tr>
              ) : (
                <></>
              )
            )}
          </tbody>
        </table>
      </div>
    </>
  );
}
