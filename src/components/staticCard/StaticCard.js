import React from "react";
import "./staticCard.css";
import Grid from "@mui/material/Grid";
import * as MdIcons from "react-icons/md";
import * as ImIcons from "react-icons/im";
import * as IoIcons from "react-icons/io5";
import * as BsIcons from "react-icons/bs";


const icons = 
  [
  {
    title: 'Masivo', 
    icon: <BsIcons.BsFillHouseFill/>,
    colorIni: "#49a3f1",
    colorFin: "#1A73E8",
  },
{
    title: 'Empresarial', 
    icon: <MdIcons.MdCorporateFare />,
    colorIni: "#de5050",
    colorFin: "#c14444",
  },
  {
    title: 'Script', 
    icon: <ImIcons.ImDatabase />,
    colorIni: "#e37b1f",
    colorFin: "#fc9030",
  },
  {
    title: 'Correctivo', 
    icon: <IoIcons.IoBandageSharp />,
    colorIni: "#dc4af0",
    colorFin: "#c42fd7",
  },
  {
    title: 'Sin Modificacion', 
    icon: <MdIcons.MdEditOff />,
    colorIni: "#4a4a49",
    colorFin: "#2a2a29",
  },
  {
    title: 'Relacionado', 
    icon: <ImIcons.ImTree />,
    colorIni: "#66BB6A",
    colorFin: "#43A047",
  },

  {
    title: 'Total desarrollos', 
    icon: <BsIcons.BsFillHouseFill/>,
    colorIni: "#49a3f1",
    colorFin: "#1A73E8",
  },
{
    title: 'Pruebas reportadas', 
    icon: <MdIcons.MdCorporateFare />,
    colorIni: "#de5050",
    colorFin: "#c14444",
  },
  {
    title: 'Pruebas generadas', 
    icon: <ImIcons.ImDatabase />,
    colorIni: "#e37b1f",
    colorFin: "#fc9030",
  },
  {
    title: 'Pruebas internas', 
    icon: <ImIcons.ImDatabase />,
    colorIni: "#e37b1f",
    colorFin: "#fc9030",
  }
];

export default function StaticCard({ data }) {

  return (
  
      data.map((dato) => (
        <Grid item xs={12} md={6} lg={4} xl={3.5} key={dato.title}>
          <div className="root-card">
            <div className="cont-info-card">
              <div className="cont-card-data">
                <div className="card-icon"  style={{background: `linear-gradient(195deg, ${icons.find( e => dato.titulo.toUpperCase() === e.title.toUpperCase()).colorIni}, ${icons.find( e => dato.titulo.toUpperCase() === e.title.toUpperCase()).colorFin})`}} >
                  {icons.find( e => dato.titulo.toUpperCase() === e.title.toUpperCase()).icon}
                </div>
                <div className="card-data"  >
                  <h4>{dato.titulo}</h4>
                  <span className="span-cantidad">{dato.cantidad} </span> {/*titulo*/}
                </div>
              </div>
              <hr></hr>
              <div className="card-extra-data">
                <p>Este valor corresponde al 10% de IMs</p>
              </div>
            </div>
          </div>
        </Grid>
      ))
  );
}
