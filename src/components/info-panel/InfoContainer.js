import React from "react";

export default function InfoContainer({ items }) {
  return (
    <div className="info-container">
      <h3>Datos Generales</h3>
      <ul>
        {
        items.map((item, index) => (
          <li key={index}>
            <p>{item}</p>
          </li>
        ))}
      </ul>
    </div>
  );
}
