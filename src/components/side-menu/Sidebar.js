import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { IconContext } from "react-icons";
import { SidebarData } from './SidebarData';
import "./sideMenu.css";
import MenuButton from "./MenuButton"; 
import logo from '../../assets/images/logo.png'; // with import

function AsideMenu() {

    const[itemSelected, setItemSelected] = useState(SidebarData[0]);
    const [open, setOpen] = useState(false);
  

    async function handleOnclick(item){
        console.log('ITEM: ', item);
        setItemSelected(item);
    }

    const abrirMenu = () => {
        setOpen(!open);
      };

    return (
        <div className="sidebar-cont">
            <IconContext.Provider value={{ color: "#fff" }}>
                <nav className="nav-menu">
                    <ul className="nav-menu-items" >
                        <Link key='home' to='/' value='Home'>
                                <li key='Home' className='nav-img'>
                                    <img src={logo} className="logo" alt="logo"></img>
                                </li>
                        </Link>
                        {SidebarData.map((item, index) => {
                        return (
                            <Link key={index} to={item.path} value={item} onClick={() => handleOnclick(item)} className={item  === itemSelected ? 'active':'' + " nav-link"} >
                                <li key={item.title}  className={item.cName}  >
                                        {item.icon}
                                        <span>{item.title}</span>                   
                                </li>
                            </Link>
                        );
                        })}
                    </ul>
                </nav>
            </IconContext.Provider>
            <MenuButton></MenuButton>
    </div>
    );
}

export default AsideMenu;
