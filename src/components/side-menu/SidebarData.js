import React from 'react';
import * as IoIcons from 'react-icons/io';
import * as MdIcons from 'react-icons/md';

export const SidebarData = [
  {
    title: 'Incidentes',
    path: '/incidencias',
    icon: <IoIcons.IoIosPaper />,
    cName: 'nav-text'
  },
  {
    title: 'Desarrollos',
    path: '/desarrollos',
    icon: <MdIcons.MdOutlineComputer />,
    cName: 'nav-text'
  }
];