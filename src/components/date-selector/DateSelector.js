import React, { useContext } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { DatePicker } from "@material-ui/pickers";
import { DateContext } from "../../context/dateContext/DateContext";
import "./DateSelector.css";

function SelectorFecha() {
  const dateContext = useContext(DateContext);

  return (
    <div className="date-container">
      <DatePicker
        views={["year", "month"]}
        label="Desde"
        minDate={new Date("2020-02-02")}
        value={dateContext.initialDate}
        onChange={(newValue) => {
          console.log("VALOR A CAMBIAR", newValue)
          newValue.setHours(0, 0, 0, 0);
          newValue.setDate(1)
          dateContext.changeInitialDate(newValue);
        }}
      />

      <DatePicker
        views={["year", "month"]}
        label="Hasta"
        minDate={new Date("2020-02-02")}
        value={dateContext.finalDate}
        onChange={(newValue) => {
          newValue.setHours(0, 0, 0, 0);
          newValue.setDate(1)
          dateContext.changeFinalDate(newValue);
        }}
      />
    </div>
  );
}

export default SelectorFecha;
