import { Button } from '@mui/material';
import React from 'react';

const style = {
    display: "flex",
    flexDirection: "row-reverse",
    width: "99%",

}

function BtnExportarExcel(){
    return (
        <div className="btn-exportarExcel" style={style}>
            <Button variant="contained" >Exportar a excel</Button>
        </div>
    );
};

export default BtnExportarExcel;