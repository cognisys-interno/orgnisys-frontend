import React from "react";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import { desarrollos } from "../../consts/constTemporal";
import * as TabNames from "./TabNames";
import "./tabs.css";
import TablaIncidentes from "../table/TableIncidente";
import TablaDesarrollo from "../table/TableDesarrollo";

//TODO, ELIMINAR ESTA CONSTANTE Y RECUPERAR LOS DATOS AL CAMBIAR DE OPCION EN EL TABS
const incidentes = [
  {
    incidencia: "IM01073666",
    tipo: "MASIVO",
    metodo: "SIN MODIFICACIONES",
    solucion:
      "La orden se encontraba provisionada sin un equipo asignado y no era posible realizar mas provisiones, por lo que se genero un script para desasignar el equipo (en caso de que haya faltado algun paso) y volver a asignarlo a la orden",
  },
  {
    incidencia: "IM01065923",
    tipo: "CORPORATIVO",
    metodo: "SIN MODIFICACIONES",
    solucion:
      "La orden se encontraba provisionada sin un equipo asignado y no era posible realizar mas provisiones, por lo que se genero un script para desasignar el equipo (en caso de que haya faltado algun paso) y volver a asignarlo a la orden",
  },
  {
    incidencia: "IM01073666",
    tipo: "CORPORATIVO",
    metodo: "CORRECTIVO",
    solucion:
      "Se reporta un splitter Ufinet que deberia de haber sido eliminado al darse de Baja de la ultima orden en la que estuvo asignado, se solicito buscar casos similares, y se genero un script para regularizar la situacion",
  },
  {
    incidencia: "IM01073666",
    tipo: "MASIVO",
    metodo: "SCRIPT",
    solucion:
      "Se reporta un splitter Ufinet que deberia de haber sido eliminado al darse de Baja de la ultima orden en la que estuvo asignado, se solicito buscar casos similares, y se genero un script para regularizar la situacion",
  },
  {
    incidencia: "IM01073666",
    tipo: "MASIVO",
    metodo: "SCRIPT",
    solucion:
      "Se reporta un splitter Ufinet que deberia de haber sido eliminado al darse de Baja de la ultima orden en la que estuvo asignado, se solicito buscar casos similares, y se genero un script para regularizar la situacion",
  },
  {
    incidencia: "IM01073666",
    tipo: "MASIVO",
    metodo: "SCRIPT",
    solucion:
      "Se reporta un splitter Ufinet que deberia de haber sido eliminado al darse de Baja de la ultima orden en la que estuvo asignado, se solicito buscar casos similares, y se genero un script para regularizar la situacion",
  },
  {
    incidencia: "IM01073666",
    tipo: "MASIVO",
    metodo: "SCRIPT",
    solucion:
      "Se reporta un splitter Ufinet que deberia de haber sido eliminado al darse de Baja de la ultima orden en la que estuvo asignado, se solicito buscar casos similares, y se genero un script para regularizar la situacion",
  },
  {
    incidencia: "IM01073666",
    tipo: "MASIVO",
    metodo: "SCRIPT",
    solucion:
      "Se reporta un splitter Ufinet que deberia de haber sido eliminado al darse de Baja de la ultima orden en la que estuvo asignado, se solicito buscar casos similares, y se genero un script para regularizar la situacion",
  },
  {
    incidencia: "IM01073666",
    tipo: "MASIVO",
    metodo: "SCRIPT",
    solucion:
      "Se reporta un splitter Ufinet que deberia de haber sido eliminado al darse de Baja de la ultima orden en la que estuvo asignado, se solicito buscar casos similares, y se genero un script para regularizar la situacion",
  },
  {
    incidencia: "IM01073666",
    tipo: "MASIVO",
    metodo: "SCRIPT",
    solucion:
      "Se reporta un splitter Ufinet que deberia de haber sido eliminado al darse de Baja de la ultima orden en la que estuvo asignado, se solicito buscar casos similares, y se genero un script para regularizar la situacion",
  },
  {
    incidencia: "IM01073666",
    tipo: "MASIVO",
    metodo: "SCRIPT",
    solucion:
      "Se reporta un splitter Ufinet que deberia de haber sido eliminado al darse de Baja de la ultima orden en la que estuvo asignado, se solicito buscar casos similares, y se genero un script para regularizar la situacion",
  },
  {
    incidencia: "IM01073666",
    tipo: "MASIVO",
    metodo: "SCRIPT",
    solucion:
      "Se reporta un splitter Ufinet que deberia de haber sido eliminado al darse de Baja de la ultima orden en la que estuvo asignado, se solicito buscar casos similares, y se genero un script para regularizar la situacion",
  },
  {
    incidencia: "IM01073666",
    tipo: "MASIVO",
    metodo: "SCRIPT",
    solucion:
      "Se reporta un splitter Ufinet que deberia de haber sido eliminado al darse de Baja de la ultima orden en la que estuvo asignado, se solicito buscar casos similares, y se genero un script para regularizar la situacion",
  },
];

const columnasIncidente = ["Incidencias", "Tipo", "Solucion"]; //TODO CREAR CONSTANTES
const columnasDesarrollo = ["Nombre", "Encargado","Tareas", "Fecha",  "Bitacora"]; //TODO CREAR CONSTANTES

function TabMenu(props) {
  const [value, setValue] = React.useState(props.tabs[0]);

  const handleChange = (event, newValue) => {
    //TODO LLAMAR METODO QUE ACTUALICE TABLA SEGUN TAB SELECCIONADO
    setValue(newValue);
  };

  return (
    <div className="main-cont-tabs">
      <TabContext value={value}>
        <Box
          sx={{ borderBottom: 1, borderColor: "divider" }}
          className="cont-tabs"
        >
          <TabList onChange={handleChange} aria-label="tabs">
            {props.tabs.map((tab, index) => (
              <Tab label={tab} value={tab} key={index} />
            ))}
          </TabList>
        </Box>
        {props.tabs.map((tab, index) => (
          <TabPanel
            className="tabla"
            value={tab}
            style={{ height: "90%" }}
            key={index}
          >
            {tab === TabNames.CORRECTIVO ||
            tab === TabNames.SCRIPT ||
            tab === TabNames.SINMODIFICACIONES ? (
              <TablaIncidentes
                columnas={columnasIncidente}
                incidentes={incidentes}
                tab={tab}
              />
            ) : (
              <TablaDesarrollo
                columnas={columnasDesarrollo}
                desarrollos={desarrollos}
                tab={tab}
              />
            )}
          </TabPanel>
        ))}
      </TabContext>
    </div>
  );
}

export default TabMenu;
