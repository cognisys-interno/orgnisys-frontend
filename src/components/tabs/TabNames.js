export const SCRIPT = "SCRIPT";
export const CORRECTIVO = "CORRECTIVO";
export const  SINMODIFICACIONES = "SIN MODIFICACIONES";
export const ANALISISYDISENIO = "ANALISISYDISENIO";
export const DESARROLLO = "DESARROLLO";
export const TESTING = "TESTING";


export function getTabsIncidencia(){
    return [SCRIPT,CORRECTIVO,SINMODIFICACIONES]
}

export function getTabsDesarrollo(){
    return [ANALISISYDISENIO,DESARROLLO,TESTING]
}
