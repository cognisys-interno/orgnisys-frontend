
export function getItemsDesarrollo(){
    return ['Pruebas reportadas:','Pruebas ok:','Pruebas generadas:','Pruebas internas:','Horas estimadas:','Horas reales:']
}

export function getItemsIncidencia(){
    return ['Masivos:','Empresariales:','Ordinarios:','Relacionados:','Correctivos:','Resueltos por script:','Sin modificaciones:'];
}