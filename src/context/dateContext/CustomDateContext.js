import React, { useState } from 'react'
import { DateContext } from './DateContext';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from 'react-toastify';


const defaultInitialDate = new Date();
const defaultFinalDate = new Date();



export const CustomDateContext = ({defaultValue = defaultInitialDate, children }) => {

    const[initialDate,setInitialDate] = useState(defaultInitialDate);
    const[finalDate,setFinalDate] = useState(defaultFinalDate);

    const changeInitialDate = (newDate) => {
        setInitialDate(newDate);
    }

    const changeFinalDate = (newDate) => {
        if(newDate < initialDate){
            toast.warn(`La fecha de fin no puede ser anterior a la inicial`, { position: toast.POSITION.TOP_RIGHT, autoClose: 5000 })
        }else{
            setFinalDate(newDate);
        }
    }
    
  return (
    <DateContext.Provider value={{initialDate, finalDate, changeFinalDate, changeInitialDate}}>{children}<ToastContainer/></DateContext.Provider>
  )
}
