import React, { useState } from 'react'

const Context = React.createContext({})

export function TarjetaContextProvider({children}) {
  const [desarrollos, setDesarrollos] = useState([])
  const [incidentes, setIncidentes] = useState([])


  return (
    <Context.Provider value={{desarrollos, setDesarrollos,incidentes,setIncidentes}}>
        {children}
    </Context.Provider>
  )
}

export default Context;