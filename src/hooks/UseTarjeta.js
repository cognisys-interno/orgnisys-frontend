import { useCallback, useContext, useState } from 'react'
import Context from '../context/TarjetasContext'
import getDesarrollos from '../services/desarrollo'
import getIncidentes from '../services/incidentes'

export default function useTarjeta() {
  
    const {incidentes,setIncidentes, desarrollos, setDesarrollos} = useContext(Context)
    const [state, setState] = useState({ loading: false, error: false})

    const obtenerIncidentes = useCallback((fechaInicial, fechaFinal) => {
        console.log("LLAMANDO A LA FUNCION",fechaInicial)
        setState({loading: true, error: false})
        getIncidentes(fechaInicial, fechaFinal)
        .then(incidentes => {
            setState({loading: false, error: false})
            setIncidentes(incidentes)
        })
        .catch( err => {
            setState({loading: false, error: true})
            console.error(err)
        })      
    }, [setIncidentes])

    const obtenerdesarrollos = useCallback((fechaInicial, fechaFinal) => {
        console.log("LLAMANDO A LA FUNCION",fechaInicial)
        setState({loading: true, error: false})
        getDesarrollos(fechaInicial, fechaFinal)
        .then(desarrollos => {
            setState({loading: false, error: false})
            setDesarrollos(desarrollos)
        })
        .catch( err => {
            setState({loading: false, error: true})
            console.error(err)
        })      
    }, [setDesarrollos])
   
    return {
        listIncidentes: incidentes,
        listDesarrollos: desarrollos,
        isLoading: state.loading,
        hasError: state.error,
        obtenerIncidentes,
        obtenerdesarrollos   
    }

}
